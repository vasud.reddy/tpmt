provider "aws"{
  region = var.region
  $profile = "${var.account}"
  #assume_role {
   # role_arn     = "arn:aws:iam::520613019447:user/vasu"
    #session_name = "SESSION_NAME01"
    #external_id  = "EXTERNAL_ID"
}

#Current Account ID
data "aws_caller_identity" "current" {}

resource "aws_iam_user_group_membership" "example1" {
  user = aws_iam_user.devops-dev.name

  groups = [
    aws_iam_group.devops.name,
    ]
}

resource "aws_iam_user_group_membership" "example2" {
  user = aws_iam_user.devops-dev.name

  groups = [
    aws_iam_group.devops.name,
  ]
}

resource "aws_iam_user" "devops-dev" {
  name = "devops-dev"
}

resource "aws_iam_group" "devops" {
  name = "devops"
}

resource "aws_iam_policy" "my_ct-idp_policy" {
  name   = "my_ct-idp_policy"
  policy = file("role-policy.json")
}

resource "aws_iam_group_policy_attachment" "test-attach" {
	    group      = aws_iam_group.devops.name
  policy_arn = aws_iam_policy.my_ct-idp_policy.arn
}
}
